import { Component } from '@angular/core';
declare var Phaser;

var that;
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  game;
  platforms; 
  player;
  ground;
  ledge;
  cursors;
  stars;
  score;
  scoreText;

  constructor() {
    this.game = new Phaser.Game(window.innerWidth, window.innerHeight, Phaser.AUTO, 'phaser-example', { preload: this.preload, create: this.create, update: this.update });
    that = this;
    this.score = 0; 
    
  }

  preload() {
    
    this.game.load.image('sky', 'assets/sky.png');
    this.game.load.image('ground', 'assets/platform.png');
    this.game.load.image('star', 'assets/star.png');
    this.game.load.spritesheet('dude', 'assets/dude.png', 32, 48);
  }

  create() {
    this.game.physics.startSystem(Phaser.Physics.ARCADE);
    this.game.add.sprite(0, 0, 'sky');
    this.platforms = this.game.add.group();
    this.platforms.enableBody = true;
    this.ground = this.platforms.create(0, this.game.world.height - 64, 'ground');
    this.ground.scale.setTo(2, 2);
    this.ground.body.immovable = true;
    this.ledge = this.platforms.create(400, 400, 'ground');
    this.ledge.body.immovable = true;
    this.ledge = this.platforms.create(-150, 250, 'ground');
    this.ledge.body.immovable = true;
    this.player = this.game.add.sprite(32, this.game.world.height - 150, 'dude');
    this.game.physics.arcade.enable(this.player);
    this.player.body.bounce.y = 0.2;
    this.player.body.gravity.y = 300;
    this.player.body.collideWorldBounds = true;

    //  Our two animations, walking left and right.
    this.player.animations.add('left', [0, 1, 2, 3], 10, true);
    this.player.animations.add('right', [5, 6, 7, 8], 10, true);
    this.cursors = this.game.input.keyboard.createCursorKeys();


    this.stars = this.game.add.group();
    this.stars.enableBody = true;

    for (var i = 0; i < 120; i++)
    {
        var star = this.stars.create(i * 7, Math.random() * 400, 'star');
        star.body.gravity.y = 600;
        star.body.bounce.y = 0.2 + Math.random() * 0.2 ;
    }

    this.scoreText = this.game.add.text(16, 16, 'Score: 0', { fontSize: '32px', fill: '#000' });
  }

 
  update() {
    var hitPlatform = this.game.physics.arcade.collide(this.player, this.platforms);
    this.game.physics.arcade.collide(this.stars, this.platforms);
    this.game.physics.arcade.overlap(this.player, this.stars,function(player,star)
    {
      star.kill(); 
      that.score += 10;
    },null,that);
    


    this.player.body.velocity.x = 0;
    if (this.cursors.left.isDown)
    {
        this.player.body.velocity.x = -150;
        this.player.animations.play('left');
    }
    else if (this.cursors.right.isDown)
    {
        this.player.body.velocity.x = 150;
        this.player.animations.play('right');
    }
    else
    {
        this.player.animations.stop();
        this.player.frame = 4;
    }

    if (this.cursors.up.isDown && this.player.body.touching.down && hitPlatform)
    {
        this.player.body.velocity.y = -350;
    }

    this.scoreText.text = 'Score: ' + that.score;

  }

}





